﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4
{
    class Matrix
    {
        // Calculation of the determinant of the matrix
        public int determinant(int[,] matrix)
        {
            int numRows = matrix.GetLength(0);
            int numCols = matrix.GetLength(1);
            int n = numCols;

            if (numCols != numRows)
            {
                throw new ArgumentException("Matrix is not square");
            }

            for (int k = 1; k < n; k++)
            {
                for (int i = k; i < n; i++)
                {
                    int C = matrix[i, k - 1] / matrix[k - 1, k - 1];
                    for (int j = 0; j < numCols; j++)
                    {
                        matrix[i, j] -= C * matrix[k - 1, j];
                    }
                }
            }

            int result = 1;
            for (int i = 0; i < n; i++)
            {
                result *= matrix[i, i];
            }
            Console.WriteLine(result);
            return result;
        }

        //matrix addition
        public int[,] Sum(int[,] firstMatrix, int[,] secondMatrix)
        {
            if (firstMatrix.GetLength(0) != secondMatrix.GetLength(0))
                throw new Exception();

            if (firstMatrix.GetLength(1) != secondMatrix.GetLength(1))
                throw new Exception();

            int[,] result = new int[firstMatrix.GetLength(0), firstMatrix.GetLength(1)];

            for (int i = 0; i < firstMatrix.GetLength(0); i++)
                for (int j = 0; j < firstMatrix.GetLength(1); j++)
                {
                    result[i, j] = (firstMatrix[i, j] + secondMatrix[i, j]);
                    Console.WriteLine(result[i, j]);
                }
            return result;
        }

        //matrix subtraction
        public int[,] Subtraction(int[,] firstMatrix, int[,] secondMatrix)
        {
            if (firstMatrix.GetLength(0) != secondMatrix.GetLength(0))
                throw new Exception();

            if (firstMatrix.GetLength(1) != secondMatrix.GetLength(1))
                throw new Exception();

            int[,] result = new int[firstMatrix.GetLength(0), firstMatrix.GetLength(1)];

            for (int i = 0; i < firstMatrix.GetLength(0); i++)
                for (int j = 0; j < firstMatrix.GetLength(1); j++)
                {
                    result[i, j] = (firstMatrix[i, j] - secondMatrix[i, j]);
                    Console.WriteLine(result[i, j]);
                }
            return result;
        }

        //matrix multiplication
        public int[,] Multiplication(int[,] firstMatrix, int[,] secondMatrix)
        {
            if (firstMatrix.GetLength(0) != secondMatrix.GetLength(1))
                throw new Exception();

            int[,] result = new int[firstMatrix.GetLength(0), secondMatrix.GetLength(1)];

            for (int i = 0; i < firstMatrix.GetLength(0); i++)
                for (int j = 0; j < secondMatrix.GetLength(1); j++)
                    for (int k = 0; k < firstMatrix.GetLength(0); k++)
                    {
                        result[i, j] = (result[i, j] + (firstMatrix[i, k] * secondMatrix[k, j]));
                        Console.WriteLine(result[i, j]);
                    }
            return result;
        }

    }
}
